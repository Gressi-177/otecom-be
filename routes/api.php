<?php

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/user')->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
});

Route::apiResource('/slider', \App\Http\Controllers\Api\SliderController::class);

Route::get("products", 'App\Http\Controllers\Api\ProductController@getProducts');
Route::get("product/{slug}", [ProductController::class, 'getProductBySlug']);
Route::get("/products/search", 'App\Http\Controllers\Api\ProductController@searchProducts');
Route::get("categories", '\App\Http\Controllers\Api\CategoryController@getCategories');
