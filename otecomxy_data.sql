-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th10 21, 2022 lúc 09:19 AM
-- Phiên bản máy phục vụ: 5.7.33
-- Phiên bản PHP: 8.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `otecomxy_data`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=visible,1=hidden',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `image_name`, `image_url`, `meta_title`, `meta_keyword`, `meta_description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Kinh tế', 'kinh-te', 'Kinh tế', 'otecom/uploads/2022-11-19-1668842980', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_600,w_600/v1/otecom/uploads/2022-11-19-1668842980.png', 'Kinh tế', 'Kinh tế', 'Kinh tế', 0, '2022-11-19 00:16:15', '2022-11-19 00:29:42'),
(3, 'Văn học', 'van-hoc', 'Văn học', 'otecom/uploads/2022-11-19-1668842256', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_600,w_600/v1/otecom/uploads/2022-11-19-1668842256.png', 'Văn học', 'Văn học', 'Văn học', 0, '2022-11-19 00:17:39', '2022-11-19 00:17:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_10_26_154138_add_details_to_users_table', 2),
(7, '2022_10_28_132032_create_products_table', 4),
(11, '2022_11_02_020800_create_product_policies_table', 7),
(12, '2022_10_27_032340_create_categories_table', 8),
(13, '2022_10_28_133423_create_product_images_table', 9),
(14, '2022_10_29_125807_create_sliders_table', 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 2, 'myToken', '4f6e8a9c95a4062f9802e7643307b20a46cd9d2167bafabbd381517eafa92357', '[\"*\"]', NULL, NULL, '2022-11-13 08:17:32', '2022-11-13 08:17:32'),
(2, 'App\\Models\\User', 2, 'myToken', '6950cbdb8f1614b9ae7fba9bd768120e18222f4b2a6c7eaa577d5946b63fa2a0', '[\"*\"]', NULL, NULL, '2022-11-13 08:28:27', '2022-11-13 08:28:27'),
(4, 'App\\Models\\User', 1, 'myToken', '79cb2ce032f2a94847acea27e0b174343bbd4a4f30c03481b080ce4a2b0c00c1', '[\"*\"]', NULL, NULL, '2022-11-13 09:18:25', '2022-11-13 09:18:25'),
(5, 'App\\Models\\User', 1, 'myToken', 'eaf9e9df602b13f65c015b991ee6bd222f0a8a06ccc039de0e8db67139226465', '[\"*\"]', NULL, NULL, '2022-11-13 09:19:03', '2022-11-13 09:19:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `original_price` int(11) NOT NULL,
  `selling_price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `trending` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=trending, 0=not-trending',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=hidden,0=visible',
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` mediumtext COLLATE utf8mb4_unicode_ci,
  `meta_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `slug`, `small_description`, `description`, `original_price`, `selling_price`, `quantity`, `trending`, `status`, `meta_title`, `meta_keyword`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 2, 'Luật Bố Già (Tái Bản 2020)', 'luat-bo-gia-tai-ban-2020', 'Luật Bố Già (Tái Bản 2020)', '<div id=\"desc_content\" class=\"std\">\r\n		    <p style=\"text-align: justify;\">Louis Ferrante</p>\r\n\r\n<p style=\"text-align: justify;\">- Cựu thành viên gia tộc Gambino, một trong Ngũ đại Gia tộc tại Thành phố New York.</p>\r\n\r\n<p style=\"text-align: justify;\">- Được FBI đề nghị hợp tác để được giảm án, nhưng đã từ chối thẳng vì không muốn phản lại anh em.</p>\r\n\r\n<p style=\"text-align: justify;\">- Liên tục tự tìm cách kháng án từ trong tù và cuối cùng đã thành công.</p>\r\n\r\n<p style=\"text-align: justify;\">- Có kinh nghiệm đọc sách suốt 8 năm rưỡi trong nhà tù liên bang Lewisburg.</p>\r\n\r\n<p style=\"text-align: justify;\">- Hoàn lương và đã viết 3 quyển sách sau khi ra tù.</p>\r\n\r\n<p style=\"text-align: justify;\">- Tích cực tham gia các hoạt động hỗ trợ tù nhân tìm đường về với xã hội thiện lương.</p>\r\n\r\n<p style=\"text-align: justify;\">LUẬT BỐ GIÀ là quyển sách thứ hai của Ferrante, đã được chọn vào danh sách 800-CEO-Read Awards, cũng như danh sách những quyển sách kinh doanh hay nhất của Forbes. Sách đã được dịch sang 17 ngôn ngữ khác trên toàn thế giới.</p>\r\n\r\n<p style=\"text-align: justify;\">Mafia thường giải quyết mọi chuyện bằng súng ống. Nhưng rất nhiều lần, họ đạt được mục tiêu tương tự bằng cách kết bạn, gầy dựng cảm tình, rồi sau đó đơn giản hỏi xin điều họ muốn.</p>\r\n\r\n<p style=\"text-align: justify;\">Ngoài phần bạo lực dã man, Mafia cũng có hệ giá trị riêng: sống ngay thẳng, đừng hứa nếu không thể giữ lời, trả nợ cũng quan trọng không kém việc đòi nợ, tôn trọng khu vực của người khác, và đừng có mãi ôm hận thù…</p>\r\n\r\n<p style=\"text-align: justify;\">Trong băng đảng, kẻ biết trân quý những giá trị của tổ chức nhất chính là kẻ sẽ kiếm được nhiều nhất.</p>\r\n\r\n<p style=\"text-align: justify;\">Trên đường về nhà khi vừa ra tù, tôi nhìn quanh và thấy thế giới đã thay đổi rất nhiều. Bạn bè và người thân trong xe cứ luôn miệng nói, “Thứ này đã thay đổi, thứ kia đã đổi thay. Mày có nhận ra cái này không, còn cái kia thì sao? Mày sẽ ổn chứ?”</p>\r\n\r\n<p style=\"text-align: justify;\">“Thế con người có thay đổi không?” Tôi hỏi.</p>\r\n\r\n<p style=\"text-align: justify;\">“Không.”</p>\r\n\r\n<p style=\"text-align: justify;\">“Vậy thì tao sẽ ổn thôi.”</p>		    <div class=\"clear\"></div>\r\n		</div>', 150000, 127000, 100, 0, 0, 'Luật Bố Già (Tái Bản 2020)', 'Luật Bố Già (Tái Bản 2020)', 'Luật Bố Già (Tái Bản 2020)', '2022-11-20 07:47:06', '2022-11-20 08:48:41'),
(2, 2, 'Bộ Sách HBR Phát Triển Sự Nghiệp (Bộ 3 Cuốn)', 'bo-sach-hbr-phat-trien-su-nghiep-bo-3-cuon', 'Bộ Sách HBR Phát Triển Sự Nghiệp (Bộ 3 Cuốn)', '<div id=\"desc_content\" class=\"std\">\r\n		    <p style=\"margin-left: 0.1pt; text-align: justify;\"><strong>LIFELONG LEARNING – XÂY DỰNG TỔ CHỨC HỌC TẬP</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Cuốn sách bao gồm các bài viết về cách thức để bạn giữ cho các kỹ năng của mình luôn mới và phù hợp, hỗ trợ cải tiến liên tục trong đội ngũ của bạn và chuẩn bị cho mọi người trong tổ chức phát triển lâu dài.</p>\r\n\r\n<p style=\"text-align: justify;\">Cuốn sách này sẽ truyền cảm hứng cho bạn:</p>\r\n\r\n<p style=\"text-align: justify;\">Trau dồi trí tò mò không ngừng.</p>\r\n\r\n<p style=\"text-align: justify;\">Phát triển điểm mạnh của bạn và biến bản thân trở thành người không thể thiếu.</p>\r\n\r\n<p style=\"text-align: justify;\">Nuôi dưỡng tư duy phát triển trong bản thân và những người khác.</p>\r\n\r\n<p style=\"text-align: justify;\">Cung cấp phản hồi hữu ích để giúp mọi nhân viên trở nên xuất sắc.</p>\r\n\r\n<p style=\"text-align: justify;\">Biến thất bại của ngày hôm nay thành thành công của ngày mai.</p>\r\n\r\n<p style=\"text-align: justify;\">Tưởng tượng lại chương trình phát triển nhân viên của bạn.</p>\r\n\r\n<p style=\"text-align: justify;\">Xây dựng một tổ chức học tập.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Đánh giá chung về cuốn sách</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Thông tin/ Đánh giá về bản gốc: hiệu ứng truyền thông, tầm ảnh hưởng của cuốn sách, giá trị mang lại/ độ phù hợp độc giả Việt</p>\r\n\r\n<p style=\"text-align: justify;\">Sách gốc khá phù hợp với độc giả Việt vì các bài báo HBR đã rất nổi tiếng, và nội dung cũng đúng với cả doanh nghiệp nước ngoài lẫn doanh nghiệp Việt.</p>\r\n\r\n<p style=\"text-align: justify;\">Trích đánh giá của chuyên gia, tổ chức uy tín về cuốn sách</p>\r\n\r\n<p style=\"text-align: justify;\">Giải thưởng đạt được; thứ hạng trên Amazon hoặc Goodread</p>\r\n\r\n<p style=\"text-align: justify;\">Được đánh giá 4.5* trên Amazon</p>\r\n\r\n<p style=\"text-align: justify;\">Xếp hạng trên Amazon:</p>\r\n\r\n<p style=\"text-align: justify;\">#423 trong mục Sustainable Business Development</p>\r\n\r\n<p style=\"text-align: justify;\">#867 trong mục Strategy &amp; Competition</p>\r\n\r\n<p style=\"text-align: justify;\">#1057 trong mục Business &amp; Organizational Learning</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>HBR’S 10 MUSTREADS ON CREATIVITY- SÁNG TẠO DƯỚI ÁP LỰC</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Tổ chức của bạn có ủng hộ sự sáng tạo hay không? Nếu muốn trau dồi về khả năng sáng tạo trong công việc, hãy đọc 10 bài viết này. Chúng tôi đã xem qua hàng trăm bài báo trên Tạp chí Kinh doanh Harvard và chọn ra những bài báo quan trọng nhất để giúp bạn khơi dậy ngọn lửa sáng tạo trong toàn tổ chức của mình. Cuốn sách này sẽ truyền cảm hứng cho bạn: Khám phá các yếu tố của sự sáng tạo và học cách ảnh hưởng đến chúng; Khai thác tiềm năng sáng tạo của một đội đa dạng; Khuyến khích sự tò mò và thử nghiệm; Tránh đổ vỡ trong hợp tác sáng tạo; Vượt qua nỗi sợ hãi ngăn cản khả năng sáng tạo bẩm sinh của bạn; Mang những ý tưởng đột phá vào cuộc sống.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Đánh giá chung về cuốn sách</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Thông tin/ Đánh giá về bản gốc: hiệu ứng truyền thông, tầm ảnh hưởng của cuốn sách, giá trị mang lại/ độ phù hợp độc giả Việt</p>\r\n\r\n<p style=\"text-align: justify;\">Tuy cuốn này không nổi nhưng chất lượng các bài viết hay, mang tính thiết thực, sẽ không làm độc giả thất vọng.</p>\r\n\r\n<p style=\"text-align: justify;\">Trích đánh giá của chuyên gia, tổ chức uy tín về cuốn sách</p>\r\n\r\n<p style=\"text-align: justify;\">Không có. Có thể dựa vào profile các tác giả trong sách để pr</p>\r\n\r\n<p style=\"text-align: justify;\">Giải thưởng đạt được; thứ hạng trên Amazon hoặc Goodread (không có nhiều rating)</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Về tác giả</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Cuốn sách tập hợp bài viết của các chuyên gia về lĩnh vực đổi mới sáng tạo của Harvard Business Review. Trong đó nổi bật là:</p>\r\n\r\n<p style=\"text-align: justify;\">- Adam Grant(tác giả sách bán chạy #1 theo bình chọn của NY times, với 2 cuốn sách bán chạy là Think again và Originals)</p>\r\n\r\n<p style=\"text-align: justify;\">- DAVID KELLEY: nhà sáng lập của IDEO một công ty về sáng tạo và trường thiết kế Hasso Plattner của Stanford.</p>\r\n\r\n<p style=\"text-align: justify;\">- ED CATMULL: co-founder của Pixar Animation Studios, nơi sản xuất những bộ phim nổi tiếng thế giới như Chú chuột đầu bếp, Rô bốt biết yêu, Câu chuyện đồ chơi 2; Công ty Quái vật; Đi tìm Nemo</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>SỨC </strong><strong>BẬT TRONG SỰ NGHIỆP</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Tổng hợp 10 bài viết từ các chuyên gia trong lĩnh vực nhân sự để giúp bạn phát triển bản thân, tạo bước chuyển mình đúng đắn trong sự nghiệp, điều hướng những con đường vòng và sự gián đoạn không thể tránh khỏi, đồng thời biến ước mơ nghề nghiệp của bạn thành hiện thực.</p>\r\n\r\n<p style=\"text-align: justify;\">Cuốn sách này sẽ truyền cảm hứng cho bạn:</p>\r\n\r\n<p style=\"text-align: justify;\">- Xác định và tận dụng thế mạnh của bạn;</p>\r\n\r\n<p style=\"text-align: justify;\">- Trau dồi trí tò mò, kỹ năng, và kiến ​​thức bạn cần để duy trì sự phù hợp chuyên môn của mình trong tương lai;</p>\r\n\r\n<p style=\"text-align: justify;\">- Điều hướng các chuyển đổi công việc lộn xộn một cách duyên dáng;</p>\r\n\r\n<p style=\"text-align: justify;\">- Xây dựng và duy trì một mạng lưới hỗ trợ và khuyến khích sự phát triển của bạn;</p>\r\n\r\n<p style=\"text-align: justify;\">- Khôi phục ý nghĩa và niềm đam mê cho công việc của bạn;</p>\r\n\r\n<p style=\"text-align: justify;\">- Trở lại sau những thất bại nghề nghiệp lớn và nhỏ;</p>\r\n\r\n<p style=\"text-align: justify;\">- Đổi mới bản thân, ngay cả trong những thời điểm khó khăn.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Trích đoạn hay</strong></p>\r\n\r\n<p style=\"text-align: justify;\">- Một số người làm việc tốt nhất trong vai trò cấp dưới. Tướng George Patton, anh hùng quân sự vĩ đại của Mỹ trong Thế chiến II, là một ví dụ điển hình. Patton là nhà chỉ huy quân đội hàng đầu của Mỹ. Tuy nhiên khi ông được đề cử vào một vai trò chỉ huy độc lập, Tướng George Marshall, tham mưu trưởng quân đội Mỹ và có lẽ là người chọn nhân tài thành công nhất trong lịch sử Mỹ đã nói: \"Patton là người thuộc cấp tốt nhất mà quân đội Mỹ từng tôi luyện, nhưng ông ấy sẽ là người chỉ huy tồi tệ nhất.”</p>\r\n\r\n<p style=\"text-align: justify;\">- Một câu hỏi quan trọng khác là: “Tôi làm được việc khi là người ra quyết định hay khi là người tham mưu?”. Rất nhiều người làm tốt nhất ở vai trò tham mưu, nhưng không thể chịu được gánh nặng và áp lực khi đưa ra quyết định. Ngược lại, nhiều người khác cần một người tham mưu để buộc họ suy nghĩ; sau đó họ có thể đưa ra quyết định và hành động nhanh chóng, tự tin và mạnh dạn.</p>\r\n\r\n<p style=\"text-align: justify;\">- Qua nghiên cứu, tôi thấy nhiều người cố gắng theo cách tiếp cận thông thường để rồi mệt mỏi sau vài tháng, nếu không muốn nói vài năm. Nhưng bằng một cách khác mà tôi gọi là phương pháp xác định <em>bản thể trong công việc</em>, cuối cùng họ đã tìm được con đường đến với sự nghiệp hoàn toàn mới. Tất nhiên, khái niệm “bản thể trong công việc” mang hai ý nghĩa. Thứ nhất, đó là nhận thức của chúng ta về bản thân trong công việc của mình, những gì chúng ta thể hiện về bản thân cho người khác thấy; và thứ nhì, đó là cách chúng ta sống cuộc đời làm việc của mình. Khái niệm đó cũng có thể biểu thị hành động – một tiến trình nỗ lực định hình lại bản thể đó. Tôi nhận thấy xây dựng bản thể là vấn đề kỹ năng, không phải vấn đề tính cách, do đó hầu như bất kỳ ai đang tìm cách đổi mới nghề nghiệp đều có thể học được. Nhưng trước tiên, chúng ta phải sẵn sàng từ bỏ mọi thứ mình từng được dạy về việc đưa ra các quyết định nghề nghiệp đúng đắn.</p>\r\n\r\n<p style=\"text-align: justify;\">- Phiên bản có thể có của chúng ta – những hình ảnh và tưởng tượng của chúng ta về người mà mình hy vọng trở thành, nghĩ rằng mình nên trở thành, hoặc thậm chí sợ trở thành – là trọng tâm của tiến trình thay đổi nghề nghiệp. Dù sách vở nói rằng nỗi đau – một phiên bản mà chúng ta sợ trở thành hiện thực – là động lực duy nhất cho sự thay đổi, nhưng trên thực tế, nỗi đau có thể gây ra tê liệt. Chúng ta chỉ thay đổi khi có những lựa chọn thay thế hấp dẫn có thể cảm nhận, tiếp xúc và nếm trải được. Vì thế, bản thể công việc, trong thực tiễn, nhất thiết phải là một quy trình thử nghiệm, kiểm tra và tìm hiểu về những phiên bản có thể có của chúng ta.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Về tác giả</strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Harvard Business Review</strong> là điểm đến hàng đầu cho tư duy quản lý thông minh. Thông qua tạp chí hàng đầu của mình, 12 ấn bản được cấp phép quốc tế, sách từ Harvard Business Review Press, nội dung và công cụ kỹ thuật số được xuất bản trên HBR.org, Harvard Business Review cung cấp cho các chuyên gia trên khắp thế giới những hiểu biết sâu sắc và thực tiễn tốt nhất để lãnh đạo bản thân và tổ chức của họ hiệu quả hơn và để tạo ra tác động tích cực.</p>		    <div class=\"clear\"></div>\r\n		</div>', 807000, 685000, 50, 1, 0, 'Bộ Sách HBR Phát Triển Sự Nghiệp (Bộ 3 Cuốn)', 'Bộ Sách HBR Phát Triển Sự Nghiệp (Bộ 3 Cuốn)', 'Bộ Sách HBR Phát Triển Sự Nghiệp (Bộ 3 Cuốn)', '2022-11-21 00:24:42', '2022-11-21 00:24:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image_name`, `image_url`, `created_at`, `updated_at`) VALUES
(6, 1, 'otecom/uploads/2022-11-20-1668959594(3)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_600,w_600/v1/otecom/uploads/2022-11-20-1668959594%283%29.png', '2022-11-20 08:53:17', '2022-11-20 08:53:17'),
(7, 1, 'otecom/uploads/2022-11-20-1668959674(1)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_1614,w_1142/v1/otecom/uploads/2022-11-20-1668959674%281%29.png', '2022-11-20 08:54:36', '2022-11-20 08:54:36'),
(8, 1, 'otecom/uploads/2022-11-20-1668959676(2)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_1614,w_1142/v1/otecom/uploads/2022-11-20-1668959676%282%29.png', '2022-11-20 08:54:38', '2022-11-20 08:54:38'),
(9, 2, 'otecom/uploads/2022-11-21-1669015500(1)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_600,w_600/v1/otecom/uploads/2022-11-21-1669015500%281%29.png', '2022-11-21 00:25:02', '2022-11-21 00:25:02'),
(10, 2, 'otecom/uploads/2022-11-21-1669015517(1)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_1595,w_1235/v1/otecom/uploads/2022-11-21-1669015517%281%29.png', '2022-11-21 00:25:20', '2022-11-21 00:25:20'),
(11, 2, 'otecom/uploads/2022-11-21-1669015520(2)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_1587,w_1212/v1/otecom/uploads/2022-11-21-1669015520%282%29.png', '2022-11-21 00:25:23', '2022-11-21 00:25:23'),
(12, 2, 'otecom/uploads/2022-11-21-1669015523(3)', 'http://res.cloudinary.com/dicbb290i/image/upload/c_fit,h_1583,w_1217/v1/otecom/uploads/2022-11-21-1669015523%283%29.png', '2022-11-21 00:25:27', '2022-11-21 00:25:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_policies`
--

CREATE TABLE `product_policies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_policy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_policy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `return_policy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_policies`
--

INSERT INTO `product_policies` (`id`, `customer_policy`, `shipping_policy`, `return_policy`, `created_at`, `updated_at`) VALUES
(1, '<p>test</p>', '<p>test</p>', '<p>test</p>', '2022-11-01 20:21:32', '2022-11-01 20:21:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=hidden, 0=active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_as` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=user,1=admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_as`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$smURewqI9ORdVCFx1TozW.G45/MkkVr7eeZ4g/k5qqemoCC2wY872', 'YcdWWRjPw47eWGLaSPCxa21eJtMaUGa9dmfL9Y6dpmHNgx93R69GazT7Gr1S', '2022-10-26 08:09:38', '2022-10-26 08:09:38', 1),
(2, 'VietDoan', 'vietdoan@gmail.com', NULL, '$2y$10$dESGvy5cmXGk/0f0U/iZb.KAzhcDp1ncy0/f8FeFTwnAFBLWlm.ba', NULL, '2022-11-13 08:17:32', '2022-11-13 08:17:32', 0),
(3, 'Trần Viết Đoàn', 'vietdoan177@gmail.com', NULL, '$2y$10$Zm0dNGBVKWFDHaMQbW05GuRzkb6FwuXPeibJZlva0V5/QvJDDXgjC', NULL, '2022-11-19 07:01:42', '2022-11-19 07:01:42', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `product_policies`
--
ALTER TABLE `product_policies`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `product_policies`
--
ALTER TABLE `product_policies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
