<head>
    <meta charset="utf-8">
    <title>Otecom: Bookshop</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="">
    <meta property="og:type" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <link rel="icon" href="{{ asset('fe/assets/imgs/logo/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('fe/assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('fe/assets/css/custom.css') }}">
    @yield('style')
    @livewireStyles()
</head>
