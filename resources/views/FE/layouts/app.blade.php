<!DOCTYPE html>
<html class="no-js" lang="en">

@include('FE.layouts.head')

<body>
    {{-- @include('FE.layouts.header') --}}
    <livewire:header-component />

    {{ $slot }}

    @include('FE.layouts.footer')


    @include('FE.layouts.script')
</body>

</html>
