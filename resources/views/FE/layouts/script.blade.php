 <script type="text/javascript" src="https://ahachat.com/customer-chats/customer_chat_Ht3gpr9NSS638b1ad0151f0.js">
 </script>
 <!-- Vendor JS-->
 <script src="{{ asset('fe/assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/vendor/jquery-3.6.0.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/vendor/jquery-migrate-3.3.0.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/vendor/bootstrap.bundle.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/slick.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/jquery.syotimer.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/wow.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/jquery-ui.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/perfect-scrollbar.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/magnific-popup.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/select2.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/waypoints.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/counterup.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/jquery.countdown.min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/images-loaded.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/isotope.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/scrollup.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/jquery.vticker-min.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/jquery.theia.sticky.js') }}"></script>
 <script src="{{ asset('fe/assets/js/plugins/jquery.elevatezoom.js') }}"></script>
 <!-- Template  JS -->
 <script src="{{ asset('fe/assets/js/main.js?v=3.3') }}"></script>
 <script src="{{ asset('fe/assets/js/shop.js?v=3.3') }}"></script>
 <script src="https://cdn.lordicon.com/qjzruarw.js"></script>
 @yield('script')
 @livewireScripts()
