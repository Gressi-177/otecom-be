@extends('layouts.admin')

@section('content')
    @if (session('message'))
        <h2>{{ session('message') }}</h2>
    @endif
    <style>
        .info-box {
            box-shadow: 0 0 1px rgba(0, 0, 0, .125), 0 1px 3px rgba(0, 0, 0, .2);
            border-radius: .25rem;
            background-color: #fff;
            display: -ms-flexbox;
            display: flex;
            margin-bottom: 1rem;
            min-height: 80px;
            padding: .5rem;
            position: relative;
            width: 100%;
        }

        .info-box .info-box-icon {
            border-radius: .25rem;
            -ms-flex-align: center;
            align-items: center;
            display: -ms-flexbox;
            display: flex;
            font-size: 1.875rem;
            -ms-flex-pack: center;
            justify-content: center;
            text-align: center;
            width: 70px;
        }

        .info-box .info-box-content {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            -ms-flex-pack: center;
            justify-content: center;
            line-height: 1.8;
            -ms-flex: 1;
            flex: 1;
            padding: 0 10px;
            overflow: hidden;
        }

        .info-box .info-box-text {
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            color: #fff;
        }

        .info-box .info-box-number {
            display: block;
            margin-top: .25rem;
            font-weight: 700;
        }
    </style>
    <div>
        <div class="title pb-20">
            <h2 class="h3 mb-0">Admin Dashboard</h2>
        </div>

        <div class="row pb-10">
            <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
                <div class="card-box height-100-p widget-style3">
                    <div class="d-flex flex-wrap">
                        <div class="widget-data">
                            <div class="weight-700 font-24 text-dark">{{ $total_users }}</div>
                            <div class="font-14 text-secondary weight-500">
                                Total user
                            </div>
                        </div>
                        <div class="widget-icon">
                            <div class="icon" data-color="#00eccf" style="color: rgb(0, 236, 207);">
                            </div><i class="icon-copy fa fa-users" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
                <div class="card-box height-100-p widget-style3">
                    <div class="d-flex flex-wrap">
                        <div class="widget-data">
                            <div class="weight-700 font-24 text-dark">{{ $total_products }}</div>
                            <div class="font-14 text-secondary weight-500">
                                Total products
                            </div>
                        </div>
                        <div class="widget-icon">
                            <div class="icon" data-color="#ff5b5b" style="color: rgb(255, 91, 91);">
                                <i class="icon-copy fa fa-book" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
                <div class="card-box height-100-p widget-style3">
                    <div class="d-flex flex-wrap">
                        <div class="widget-data">
                            <div class="weight-700 font-24 text-dark">{{ $total_order }}</div>
                            <div class="font-14 text-secondary weight-500">
                                Total Order
                            </div>
                        </div>
                        <div class="widget-icon">
                            <div class="icon">
                                <i class="icon-copy fa fa-shopping-cart" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
                <div class="card-box height-100-p widget-style3">
                    <div class="d-flex flex-wrap">
                        <div class="widget-data">
                            <div class="weight-700 font-24 text-dark">{{ number_format($total_revenus, 0, '', ',') }}đ</div>
                            <div class="font-14 text-secondary weight-500">Total revenus</div>
                        </div>
                        <div class="widget-icon">
                            <div class="icon" data-color="#09cc06" style="color: rgb(9, 204, 6);">
                                <i class="icon-copy fa fa-money" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-8 col-md-6 mb-20">
                <div class="card-box">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" colspan="3">Latest Order</th>
                                </tr>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($latest_Order as $order)
                                    <tr>
                                        <td scope="row">{{ $order->id }}</td>
                                        <td scope="row">{{ $order->product->name }}</td>
                                        <td scope="row"><span
                                                class="badge badge-success">{{ $order->order->status }}</span>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div class="float-right" style="margin: -1rem 2rem 1rem 0;">
                            <a class="badge badge-primary" href="{{ url('admin/order') }}">View All</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12 mb-20">
                <div class="info-box mb-3 bg-warning">
                    <span class="info-box-icon"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total year revenus</span>
                        <span class="info-box-number">{{ number_format($total_year_revenus, '0', '', ',') }}đ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="info-box mb-3 bg-success">
                    <span class="info-box-icon"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total month revenus</span>
                        <span class="info-box-number">{{ number_format($total_month_revenus, '0', '', ',') }}đ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="info-box mb-3 bg-danger">
                    <span class="info-box-icon"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total week revenus</span>
                        <span class="info-box-number">{{ number_format($total_week_revenus, '0', '', ',') }}đ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="info-box mb-3 bg-info">
                    <span class="info-box-icon"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total day revenus</span>
                        <span class="info-box-number">{{ number_format($total_day_revenus, '0', '', ',') }}đ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

        </div>

    </div>
@endsection
