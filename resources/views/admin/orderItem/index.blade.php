@extends('layouts.admin')


@section('content')
    <div>
        <div class="pd-20 card-box mb-30">
            <div class="clearfix mb-20">
                <div class="pull-left">
                    <h4 class="text-blue h4">Order Detail</h4>
                    @if (session('message'))
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                </div>
                <div class="pull-right">
                    <a href="{{ route('order') }}" class="btn btn-primary btn-sm scroll-click" rel="content-y" role="button"
                        aria-expanded="true">All order</a>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col" colspan="8">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Order ID</th>
                        <td scope="row">{{ $order->id }}</td>
                        <th scope="row">Order Date</th>
                        <td scope="row">{{ $order->created_at }}</td>
                        <th scope="row">Status</th>
                        <td> <span class="badge badge-success">{{ $order->status }}</span></td>
                        @if ($order->status == 'delivered')
                            <th scope="row">Delivered Date</th>
                            <td scope="row">{{ $order->delivered_date }}</td>
                        @elseif ($order->status == 'cancelled')
                            <th scope="row">Cancel Date</th>
                            <td scope="row">{{ $order->canceled_date }}</td>
                        @endif
                    </tr>
                </tbody>
            </table>

            <h4 class="text-black h4">Product</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Image</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orderItem as $item)
                        <tr>
                            <td scope="row">{{ $item->product->name }}</td>
                            <td scope="row"><img src="{{ $item->product->productImages[0]->image_url }}" alt=""
                                    width="80px">
                            </td>
                            <td scope="row">{{ number_format($item->price, 0, '', ',') }} đ</td>
                            <td scope="row">{{ $item->quantity }}</td>
                            <td scope="row">{{ number_format($item->quantity * $item->price, 0, '', ',') }} đ</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col" colspan="2">Order sumary</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Subtotal</th>
                        <td scope="row">{{ number_format($order->subtotal, 0, '', ',') }}đ</td>
                    </tr>
                    <tr>
                        <th scope="row">Tax</th>
                        <td scope="row">{{ number_format($order->tax, 0, '', ',') }}đ</td>
                    </tr>
                    <tr>
                        <th scope="row">Subtotal</th>
                        <td scope="row">{{ number_format($order->total, 0, '', ',') }}đ</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col" colspan="2">Billing detail</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Name</th>
                        <td scope="row">{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Address</th>
                        <td scope="row">{{ $order->address }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Phone</th>
                        <td scope="row">{{ $order->phone }}</td>
                    </tr>

                </tbody>
            </table>


            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col" colspan="2">Transaction</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Transaction mode</th>
                        <td scope="row">{{ $order->transaction->mode }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Status</th>
                        <td scope="row">{{ $order->transaction->status }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Date</th>
                        <td scope="row">{{ $order->transaction->created_at }}</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
