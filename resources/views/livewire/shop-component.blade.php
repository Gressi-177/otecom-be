<div>
    <style>
        .wishlisted {
            background-color: #f15412 !important;
            border: 1px solid transparent !important;
        }

        .wishlisted i {
            color: #fff !important;
        }
    </style>
    <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="{{ route('home') }}" rel="nofollow">Home</a>
                    <span></span> Shop
                </div>
            </div>
        </div>
        <section class="mt-50 mb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="shop-product-fillter">
                            <div class="totall-product">
                                <p> We found <strong class="text-brand">{{ $products->total() }}</strong> items for you!
                                </p>
                            </div>
                            <div class="sort-by-product-area">
                                <div class="sort-by-cover mr-10">
                                    <div class="sort-by-product-wrap">
                                        <div class="sort-by">
                                            <span><i class="fi-rs-apps"></i>Show:</span>
                                        </div>
                                        <div class="sort-by-dropdown-wrap">
                                            <span> {{ $limit }} <i class="fi-rs-angle-small-down"></i></span>
                                        </div>
                                    </div>
                                    <div class="sort-by-dropdown">
                                        <ul>
                                            <li><a href="#" class="{{ $limit == 9 ? 'active' : '' }}"
                                                    wire:click.prevent="changeLimit(9)">9</a>
                                            </li>
                                            <li><a href="#" class="{{ $limit == 15 ? 'active' : '' }}"
                                                    wire:click.prevent="changeLimit(15)">15</a></li>
                                            <li><a class="{{ $limit == 21 ? 'active' : '' }}" href="#"
                                                    wire:click.prevent="changeLimit(21)">21</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="sort-by-cover">
                                    <div class="sort-by-product-wrap">
                                        <div class="sort-by">
                                            <span><i class="fi-rs-apps-sort"></i>Sort by:</span>
                                        </div>
                                        <div class="sort-by-dropdown-wrap">
                                            <span> {{ $orderBy }} <i class="fi-rs-angle-small-down"></i></span>
                                        </div>
                                    </div>
                                    <div class="sort-by-dropdown">
                                        <ul>
                                            <li><a class="{{ $orderBy == 'Default sorting' ? 'active' : '' }}"
                                                    href="#"
                                                    wire:click.prevent="changeOrderBy('Default sorting')">Default
                                                    sorting</a></li>
                                            <li><a class="{{ $orderBy == 'Price: Low to High' ? 'active' : '' }}"
                                                    href="#"
                                                    wire:click.prevent="changeOrderBy('Price: Low to High')">Price: Low
                                                    to High</a></li>
                                            <li><a class="{{ $orderBy == 'Price: High to Low' ? 'active' : '' }}"
                                                    href="#"
                                                    wire:click.prevent="changeOrderBy('Price: High to Low')">Price: High
                                                    to Low</a></li>
                                            <li><a class="{{ $orderBy == 'Sort by Newness' ? 'active' : '' }}"
                                                    href="#"
                                                    wire:click.prevent="changeOrderBy('Sort by Newness')">Sort by
                                                    Newness</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row product-grid-3">
                            @php
                                $witem = Cart::instance('wishlist')
                                    ->content()
                                    ->pluck('id');
                            @endphp
                            @foreach ($products as $product)
                                <div class="col-lg-4 col-md-4 col-6 col-sm-6">
                                    <div class="product-cart-wrap mb-30">
                                        <div class="product-img-action-wrap">
                                            <div class="product-img product-img-zoom">
                                                <a href="{{ route('shop.details', ['slug' => $product->slug]) }}">
                                                    <img class="default-img"
                                                        src="{{ $product->productImages[0]->image_url }}"
                                                        alt="">
                                                    {{-- <img class="hover-img"
                                                        src="{{ asset('fe/assets/imgs/shop/product-2-2.jpg') }}"
                                                        alt=""> --}}
                                                </a>
                                            </div>
                                            <div class="product-action-1">
                                                <a aria-label="Quick view" class="action-btn hover-up"
                                                    data-bs-toggle="modal" data-bs-target="#quickViewModal">
                                                    <i class="fi-rs-search"></i></a>
                                                @if ($witem->contains($product->id))
                                                    <a aria-label="Remove From Wishlist"
                                                        class="action-btn hover-up wishlisted" href=""
                                                        wire:click.prevent="removeFromWishlist({{ $product->id }})"><i
                                                            class="fi-rs-heart"></i></a>
                                                @else
                                                    <a aria-label="Add To Wishlist" class="action-btn hover-up"
                                                        href=""
                                                        wire:click.prevent="addToWishlist({{ $product->id }}, '{{ $product->name }}',{{ $product->selling_price }})"><i
                                                            class="fi-rs-heart"></i></a>
                                                @endif
                                                <a aria-label="Compare" class="action-btn hover-up"
                                                    href="compare.php"><i class="fi-rs-shuffle"></i></a>
                                            </div>
                                            @if ($product->selling_price)
                                                <div class="product-badges product-badges-position product-badges-mrg">
                                                    <span class="hot">Hot</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="product-content-wrap">
                                            <div class="product-category">
                                                <a href="shop.html">{{ $product->category->name }}</a>
                                            </div>
                                            <h2><a
                                                    href="{{ route('shop.details', ['slug' => $product->slug]) }}">{{ $product->name }}</a>
                                            </h2>
                                            <div class="rating-result">
                                                @if ($product->selling_price)
                                                    <span>
                                                        <span>{{ ceil((($product->original_price - $product->selling_price) * 100) / $product->original_price) }}
                                                            %</span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="product-price">
                                                @if ($product->selling_price < $product->original_price)
                                                    <span>{{ number_format($product->selling_price, 0, '', ',') }}
                                                        đ</span>
                                                    <span
                                                        class="old-price">{{ number_format($product->original_price, 0, '', ',') }}
                                                        đ</span>
                                                @else
                                                    <span>{{ number_format($product->original_price, 0, '', ',') }} đ
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="product-action-1 show">
                                                <a aria-label="Add To Cart" class="action-btn hover-up" href="#"
                                                    wire:click.prevent="store({{ $product->id }}, '{{ $product->name }}',{{ $product->selling_price }})"><i
                                                        class="fi-rs-shopping-bag-add"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <!--pagination-->
                        <div class="pagination-area mt-15 mb-sm-5 mb-lg-0">
                            {{ $products->links('livewire.custom-pagination-links-view') }}
                        </div>
                    </div>
                    <div class="col-lg-3 primary-sidebar sticky-sidebar">
                        <div class="row">
                            <div class="col-lg-12 col-mg-6"></div>
                            <div class="col-lg-12 col-mg-6"></div>
                        </div>
                        <div class="widget-category mb-30">
                            <h5 class="section-title style-1 mb-30 wow fadeIn animated">Category</h5>
                            <ul class="categories">
                                @foreach ($categories as $category)
                                    <li><a
                                            href="{{ route('product.category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Fillter By Price -->
                        <div class="sidebar-widget price_range range mb-30">
                            <div class="list-group">
                                <div class="list-group-item mb-10 mt-10">
                                    <label class="fw-900">Price</label>
                                    <div class="custome-checkbox">
                                        <input class="form-check-input" type="checkbox" name="checkbox"
                                            id="exampleCheckbox11" value="" onclick="onlyOne(this)">
                                        <label wire:click="fillByPrice(0,100000)" class="form-check-label"
                                            for="exampleCheckbox11"><span>
                                            </span>0đ - 100,000đ</label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="checkbox"
                                            id="exampleCheckbox21" value="" onclick="onlyOne(this)">
                                        <label wire:click="fillByPrice(100000,200000)" class="form-check-label"
                                            for="exampleCheckbox21">100,000đ -
                                            200,000đ<span>
                                            </span></label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="checkbox"
                                            id="exampleCheckbox22" value="" onclick="onlyOne(this)">
                                        <label wire:click="fillByPrice(200000,300000)" class="form-check-label"
                                            for="exampleCheckbox22">200,000đ -
                                            300,000đ<span>
                                            </span></label>
                                        <br>
                                        <input class="form-check-input" type="checkbox" name="checkbox"
                                            id="exampleCheckbox23" value="" onclick="onlyOne(this)">
                                        <label wire:click="fillByPrice(300000,'max')" class="form-check-label"
                                            for="exampleCheckbox23">300,000đ -
                                            Above<span>
                                            </span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product sidebar Widget -->
                        <div class="sidebar-widget product-sidebar  mb-30 p-30 bg-grey border-radius-10">
                            <div class="widget-header position-relative mb-20 pb-10">
                                <h5 class="widget-title mb-10">New products</h5>
                                <div class="bt-1 border-color-1"></div>
                            </div>
                            @foreach ($nproducts as $product)
                                <div class="single-post clearfix">
                                    <div class="image">
                                        <img src="{{ $product->productImages[0]->image_url }}" alt="#">
                                    </div>
                                    <div class="content pt-10">
                                        <h5><a
                                                href="{{ route('shop.details', ['slug' => $product->slug]) }}">{{ $product->name }}</a>
                                        </h5>
                                        <p class="price mb-0 mt-5">{{ $product->selling_price }} đ</p>
                                        <div class="product-rate">
                                            <div class="product-rating" style="width:90%"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <script>
        function onlyOne(checkbox) {
            var checkboxes = document.getElementsByName('checkbox')
            checkboxes.forEach((item) => {
                if (item !== checkbox) item.checked = false
            })
        }
    </script>
</div>
