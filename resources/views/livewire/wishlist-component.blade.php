<div>
    <style>
        .wishlisted {
            background-color: #f15412 !important;
            border: 1px solid transparent !important;
        }

        .wishlisted i {
            color: #fff !important;
        }
    </style>
    <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="{{ route('home') }}" rel="nofollow">Home</a>
                    <span></span> Wishlist
                </div>
            </div>
        </div>
        <section class="mt-50 mb-50">
            <div class="container">
                <div class="row product-grid-4">
                    @php
                        $witem = Cart::instance('wishlist')
                            ->content()
                            ->pluck('id');
                    @endphp
                    @foreach (Cart::instance('wishlist')->content() as $item)
                        <div class="col-lg-3 col-md-4 col-6 col-sm-6">
                            <div class="product-cart-wrap mb-30">
                                <div class="product-img-action-wrap">
                                    <div class="product-img product-img-zoom">
                                        <a href="{{ route('shop.details', ['slug' => $item->model->slug]) }}">
                                            <img class="default-img"
                                                src="{{ $item->model->productImages[0]->image_url }}" alt="">
                                            {{-- <img class="hover-img"
                                                        src="{{ asset('fe/assets/imgs/shop/product-2-2.jpg') }}"
                                                        alt=""> --}}
                                        </a>
                                    </div>
                                    <div class="product-action-1">
                                        <a aria-label="Quick view" class="action-btn hover-up" data-bs-toggle="modal"
                                            data-bs-target="#quickViewModal">
                                            <i class="fi-rs-search"></i></a>
                                        @if ($witem->contains($item->model->id))
                                            <a aria-label="Remove From Wishlist" class="action-btn hover-up wishlisted"
                                                href=""
                                                wire:click.prevent="removeFromWishlist({{ $item->model->id }})"><i
                                                    class="fi-rs-heart"></i></a>
                                        @else
                                            <a aria-label="Add To Wishlist" class="action-btn hover-up" href=""
                                                wire:click.prevent="addToWishlist({{ $item->model->id }}, '{{ $item->model->name }}',{{ $item->model->selling_price }})"><i
                                                    class="fi-rs-heart"></i></a>
                                        @endif
                                        <a aria-label="Compare" class="action-btn hover-up" href="compare.php"><i
                                                class="fi-rs-shuffle"></i></a>
                                    </div>
                                    @if ($item->model->selling_price)
                                        <div class="product-badges product-badges-position product-badges-mrg">
                                            <span class="hot">Hot</span>
                                        </div>
                                    @endif
                                </div>
                                <div class="product-content-wrap">
                                    <div class="product-category">
                                        <a href="shop.html">{{ $item->model->category->name }}</a>
                                    </div>
                                    <h2><a
                                            href="{{ route('shop.details', ['slug' => $item->model->slug]) }}">{{ $item->model->name }}</a>
                                    </h2>
                                    <div class="rating-result">
                                        @if ($item->model->selling_price)
                                            <span>
                                                <span>{{ ceil((($item->model->original_price - $item->model->selling_price) * 100) / $item->model->original_price) }}
                                                    %</span>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="product-price">
                                        @if ($item->model->selling_price < $item->model->original_price)
                                            <span>{{ number_format($item->model->selling_price, 0, '', ',') }}
                                                đ</span>
                                            <span
                                                class="old-price">{{ number_format($item->model->original_price, 0, '', ',') }}
                                                đ</span>
                                        @else
                                            <span>{{ number_format($item->model->original_price, 0, '', ',') }} đ
                                            </span>
                                        @endif
                                    </div>

                                    <div class="product-action-1 show">
                                        <a aria-label="Add To Cart" class="action-btn hover-up" href="#"
                                            wire:click.prevent="store({{ $item->model->id }}, '{{ $item->model->name }}',{{ $item->model->selling_price }})"><i
                                                class="fi-rs-shopping-bag-add"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </main>
</div>
