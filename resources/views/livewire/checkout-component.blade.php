<div>
    <main class="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow">Home</a>
                    <span></span> Shop
                    <span></span> Checkout
                </div>
            </div>
        </div>
        <section class="mt-50 mb-50">
            <div class="container">

                <form wire:submit.prevent="placeOrder">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-25">
                                <h4>Shipping Details</h4>
                            </div>
                            <form method="post">
                                <div class="form-group">
                                    <input type="text" required placeholder="Full name *" wire:model="name">
                                </div>

                                <div class="form-group">
                                    <input type="text" placeholder="Address *" wire:model="address"
                                        value="{{ Auth::user()->address ? Auth::user()->address : '' }}">

                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Phone *" wire:model="phone">
                                </div>
                                <div class="mb-20">
                                    <h5>Additional information</h5>
                                </div>
                                <div class="form-group mb-30">
                                    <textarea rows="5" placeholder="Order notes" wire:model="note"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="order_review">
                                <div class="mb-20">
                                    <h4>Your Orders</h4>
                                </div>
                                <div class="table-responsive order_table text-center">
                                    <table class="table">

                                        @if (Session::has('checkout'))
                                            <tr>
                                                <th>Sub total</th>
                                                <td colspan="2" class="product-subtotal"><span
                                                        class="font-xl text-brand fw-900">{{ Session::get('checkout')['subtotal'] }}
                                                        đ</span></td>
                                            </tr>
                                            <tr>
                                                <th>Tax</th>
                                                <td colspan="2" class="product-subtotal"><span
                                                        class="font-xl text-brand fw-900">{{ Session::get('checkout')['tax'] }}
                                                        đ</span></td>
                                            </tr>
                                            <tr>
                                                <th>Total</th>
                                                <td colspan="2" class="product-subtotal"><span
                                                        class="font-xl text-brand fw-900">{{ Session::get('checkout')['total'] }}
                                                        đ</span></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="payment_method">
                                    <div class="mb-25">
                                        <h5>Payment</h5>
                                    </div>
                                    <div class="payment_option">
                                        <div class="custome-radio">
                                            <input class="form-check-input" type="radio" name="payment_option"
                                                id="exampleRadios3" value="cod" wire:model="paymentmode">
                                            <label class="form-check-label" for="exampleRadios3"
                                                data-bs-toggle="collapse" data-target="#cashOnDelivery"
                                                aria-controls="cashOnDelivery">Cash On Delivery</label>
                                        </div>
                                        <div class="custome-radio">
                                            <input class="form-check-input" type="radio" name="payment_option"
                                                id="exampleRadios4" value="card" wire:model="paymentmode">
                                            <label class="form-check-label" for="exampleRadios4"
                                                data-bs-toggle="collapse" data-target="#cardPayment"
                                                aria-controls="cardPayment">Card Payment</label>
                                        </div>
                                        <div class="custome-radio">
                                            <input class="form-check-input" type="radio" name="payment_option"
                                                id="exampleRadios5" value="paypal" wire:model="paymentmode">
                                            <label class="form-check-label" for="exampleRadios5"
                                                data-bs-toggle="collapse" data-target="#paypal"
                                                aria-controls="paypal">Paypal</label>
                                        </div>
                                    </div>
                                    {{-- @error('paymentmode')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror --}}
                                </div>
                                <button type="submit" class="btn btn-fill-out btn-block mt-30">Place Order</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
</div>
