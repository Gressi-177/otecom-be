<div>
    <div class="container mt-30">
        <div class="pd-20 card-box mb-30">
            <div class="clearfix mb-20">
                <div class="pull-left">
                    <h4 class="text-blue h4">Order</h4>
                    @if (session('message'))
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                </div>
            </div>
            @if (Session::has('order_message'))
                <div class="alert alert-success" role="alert">{{ Session::get('order_message') }}</div>
            @endif
            @if (count($orders))
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Sub total</th>
                            <th scope="col">Tax</th>
                            <th scope="col">Total</th>
                            <th scope="col">Username</th>
                            <th scope="col">Address</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Status</th>
                            <th scope="col">Order Date</th>
                            <th scope="col" colspan="2" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <th scope="row">{{ $order->id }}</th>
                                <td>{{ number_format($order->subtotal, 0, '', ',') }}đ </td>
                                <td>{{ number_format($order->tax, 0, '', ',') }}đ </td>
                                <td>{{ number_format($order->total, 0, '', ',') }}đ </td>
                                <td>{{ $order->name }}</td>
                                <td>{{ $order->address }}</td>
                                <td>{{ $order->phone }}</td>
                                <td> <span class="badge bg-warning text-dark">{{ $order->status }}</span> </td>
                                <td>{{ $order->created_at }}</td>
                                <td>
                                    <a href="{{ route('user.details', ['order_id' => $order->id]) }}" type="button"
                                        class="btn btn-sm btn-info">
                                        View
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                <div>{{ $orders->links() }}</div>
            @else
                <div style="display: flex;justify-content: center;flex-direction: column;align-items: center;">
                    <h2>You don't have any orders</h2>
                    <lord-icon src="https://cdn.lordicon.com/lpddubrl.json" trigger="loop" delay="500"
                        style="width:350px;height:350px">
                    </lord-icon>
                    <a href="{{ route('shop') }}" class="btn btn-submit btn-submitx">Continue Shopping</a>
                </div>
            @endif
        </div>
    </div>
</div>

</div>
