<div>
    <div class="header-action-icon-2">
        <a class="mini-cart-icon" href="{{ route('shop.cart') }}">
            <img alt="Cart" src="{{ asset('fe/assets/imgs/theme/icons/icon-cart.svg') }}">
            @if (Cart::instance('cart')->count() > 0)
                <span class="pro-count blue">{{ Cart::instance('cart')->count() }}</span>
            @endif
        </a>
    </div>
</div>
