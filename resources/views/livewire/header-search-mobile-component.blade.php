<div>
    <form action="{{ route('product.search') }}">
        <input type="text" name="q" placeholder="Search for items...">
        <button type="submit"><i class="fi-rs-search"></i></button>
    </form>
</div>
