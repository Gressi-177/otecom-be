<div>
    <main class="main">
        <section class="home-slider position-relative pt-50">
            <div class="hero-slider-1 dot-style-1 dot-style-1-position-1">
                @foreach ($sliders as $slider)
                    <div class="single-hero-slider single-animation-wrap">
                        <div class="container" style="display: flex;justify-content:center">
                            <div>
                                <img src="{{ $slider->image_url }}" alt="" style="border-radius: 10px"
                                    width="1000px">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{-- <div class="slider-arrow hero-slider-1-arrow"></div> --}}
        </section>
        <section class="featured section-padding position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6 mb-sm-3 mb-md-3 mb-lg-0">
                        <div class="banner-features wow fadeIn animated hover-up">
                            <img src="{{ asset('fe/assets/imgs/theme/icons/feature-1.png') }}" alt="">
                            <h4 class="bg-1">Free Shipping</h4>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6 mb-sm-3 mb-md-3 mb-lg-0">
                        <div class="banner-features wow fadeIn animated hover-up">
                            <img src="{{ asset('fe/assets/imgs/theme/icons/feature-2.png') }}" alt="">
                            <h4 class="bg-3">Online Order</h4>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6 mb-sm-3 mb-md-3 mb-lg-0">
                        <div class="banner-features wow fadeIn animated hover-up">
                            <img src="{{ asset('fe/assets/imgs/theme/icons/feature-3.png') }}" alt="">
                            <h4 class="bg-2">Save Money</h4>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6 mb-sm-3 mb-md-3 mb-lg-0">
                        <div class="banner-features wow fadeIn animated hover-up">
                            <img src="{{ asset('fe/assets/imgs/theme/icons/feature-4.png') }}" alt="">
                            <h4 class="bg-4">Promotions</h4>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6 mb-sm-3 mb-md-3 mb-lg-0">
                        <div class="banner-features wow fadeIn animated hover-up">
                            <img src="{{ asset('fe/assets/imgs/theme/icons/feature-5.png') }}" alt="">
                            <h4 class="bg-5">Happy Sell</h4>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6 mb-sm-3 mb-md-3 mb-lg-0">
                        <div class="banner-features wow fadeIn animated hover-up">
                            <img src="{{ asset('fe/assets/imgs/theme/icons/feature-6.png') }}" alt="">
                            <h4 class="bg-6">24/7 Support</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="product-tabs section-padding position-relative wow fadeIn animated">
            <div class="bg-square"></div>
            <div class="container">
                <div class="tab-header">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="nav-tab-two" data-bs-toggle="tab"
                                data-bs-target="#tab-two" type="button" role="tab" aria-controls="tab-two"
                                aria-selected="false">Best
                                seller</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="nav-tab-three" data-bs-toggle="tab" data-bs-target="#tab-three"
                                type="button" role="tab" aria-controls="tab-three" aria-selected="false">New
                            </button>
                        </li>
                    </ul>
                    <a href="{{ route('shop') }}" class="view-more d-none d-md-flex">View More<i
                            class="fi-rs-angle-double-small-right"></i></a>
                </div>
                <!--End nav-tabs-->
                <div class="tab-content wow fadeIn animated" id="myTabContent">

                    <div class="tab-pane fade active show" id="tab-two" role="tabpanel" aria-labelledby="tab-two">
                        <div class="row product-grid-4">
                            @foreach ($pproducts as $item)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-6">
                                    <div class="product-cart-wrap mb-30">
                                        <div class="product-img-action-wrap">
                                            <div class="product-img product-img-zoom">
                                                <a href="{{ route('shop.details', ['slug' => $item->slug]) }}">
                                                    <img class="default-img"
                                                        src="{{ $item->productImages[0]->image_url }}" alt="">
                                                    {{-- <img class="hover-img" src="assets/imgs/shop/product-1-2.jpg"
                                                        alt=""> --}}
                                                </a>
                                            </div>
                                            <div class="product-action-1">
                                                <a aria-label="Quick view" class="action-btn hover-up"
                                                    data-bs-toggle="modal" data-bs-target="#quickViewModal"><i
                                                        class="fi-rs-eye"></i></a>
                                                <a aria-label="Add To Wishlist" class="action-btn hover-up"
                                                    href="wishlist.php"><i class="fi-rs-heart"></i></a>
                                                <a aria-label="Compare" class="action-btn hover-up"
                                                    href="compare.php"><i class="fi-rs-shuffle"></i></a>
                                            </div>
                                            @if ($item->trending == 1)
                                                <div class="product-badges product-badges-position product-badges-mrg">
                                                    <span class="hot">Hot</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="product-content-wrap">
                                            <div class="product-category">
                                                <a
                                                    href="{{ route('product.category', ['slug' => $item->category->slug]) }}">{{ $item->category->name }}</a>
                                            </div>
                                            <h2><a
                                                    href="{{ route('shop.details', ['slug' => $item->slug]) }}">{{ $item->name }}</a>
                                            </h2>
                                            <div class="rating-result" title="90%">
                                                {{-- @if ($item->selling_price)
                                                    <span>
                                                        <span>{{ ceil((($item->original_price - $item->selling_price) * 100) / $item->original_price) }}
                                                            %</span>
                                                    </span>
                                                @endif --}}
                                            </div>
                                            <div class="product-price">
                                                @if ($item->selling_price)
                                                    <span>{{ number_format($item->selling_price, 0, '', ',') }}
                                                        đ</span>
                                                    <span
                                                        class="old-price">{{ number_format($item->original_price, 0, '', ',') }}
                                                        đ</span>
                                                @else
                                                    <span>{{ number_format($item->original_price, 0, '', ',') }} đ
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="product-action-1 show">
                                                <a aria-label="Add To Cart" class="action-btn hover-up"
                                                    wire:click.prevent="store({{ $item->id }}, '{{ $item->name }}',{{ $item->selling_price }})"><i
                                                        class="fi-rs-shopping-bag-add"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <!--End product-grid-4-->
                    </div>
                    <!--En tab two (Popular)-->
                    <div class="tab-pane fade" id="tab-three" role="tabpanel" aria-labelledby="tab-three">
                        <div class="row product-grid-4">
                            @foreach ($nproducts as $item)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-6">
                                    <div class="product-cart-wrap mb-30">
                                        <div class="product-img-action-wrap">
                                            <div class="product-img product-img-zoom">
                                                <a href="{{ route('shop.details', ['slug' => $item->slug]) }}">
                                                    <img class="default-img"
                                                        src="{{ $item->productImages[0]->image_url }}"
                                                        alt="">
                                                    {{-- <img class="hover-img" src="assets/imgs/shop/product-1-2.jpg"
                                                        alt=""> --}}
                                                </a>
                                            </div>
                                            <div class="product-action-1">
                                                <a aria-label="Quick view" class="action-btn hover-up"
                                                    data-bs-toggle="modal" data-bs-target="#quickViewModal"><i
                                                        class="fi-rs-eye"></i></a>
                                                <a aria-label="Add To Wishlist" class="action-btn hover-up"
                                                    href="wishlist.php"><i class="fi-rs-heart"></i></a>
                                                <a aria-label="Compare" class="action-btn hover-up"
                                                    href="compare.php"><i class="fi-rs-shuffle"></i></a>
                                            </div>
                                            @if ($item->trending == 1)
                                                <div class="product-badges product-badges-position product-badges-mrg">
                                                    <span class="hot">Hot</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="product-content-wrap">
                                            <div class="product-category">
                                                <a
                                                    href="{{ route('product.category', ['slug' => $item->category->slug]) }}">{{ $item->category->name }}</a>
                                            </div>
                                            <h2><a
                                                    href="{{ route('shop.details', ['slug' => $item->slug]) }}">{{ $item->name }}</a>
                                            </h2>
                                            <div class="rating-result" title="90%">
                                                {{-- @if ($item->selling_price)
                                                    <span>
                                                        <span>{{ ceil((($item->original_price - $item->selling_price) * 100) / $item->original_price) }}
                                                            %</span>
                                                    </span>
                                                @endif --}}
                                            </div>
                                            <div class="product-price">
                                                @if ($item->selling_price)
                                                    <span>{{ number_format($item->selling_price, 0, '', ',') }}
                                                        đ</span>
                                                    <span
                                                        class="old-price">{{ number_format($item->original_price, 0, '', ',') }}
                                                        đ</span>
                                                @else
                                                    <span>{{ number_format($item->original_price, 0, '', ',') }} đ
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="product-action-1 show">
                                                <a aria-label="Add To Cart" class="action-btn hover-up"
                                                    wire:click.prevent="store({{ $item->id }}, '{{ $item->name }}',{{ $item->selling_price }})"><i
                                                        class="fi-rs-shopping-bag-add"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <!--End product-grid-4-->
                    </div>
                    <!--En tab three (New added)-->
                </div>
                <!--End tab-content-->
            </div>
        </section>

        <section class="popular-categories section-padding mt-15 mb-25">
            <div class="container wow fadeIn animated">
                <h3 class="section-title mb-20"><span>Popular</span> Categories</h3>
                <div class="carausel-6-columns-cover position-relative">
                    <div class="slider-arrow slider-arrow-2 carausel-6-columns-arrow" id="carausel-6-columns-arrows">
                    </div>
                    <div class="carausel-6-columns" id="carausel-6-columns">
                        @foreach ($categories as $category)
                            <div class="card-1">
                                <figure class=" img-hover-scale overflow-hidden"
                                    style="justify-content: center; display: flex">
                                    <a href="{{ route('product.category', ['slug' => $category->slug]) }}">
                                        <img src="{{ $category->image_url }}" alt=""></a>
                                </figure>
                                <h5><a href="shop.html">{{ $category->name }}</a></h5>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>

    </main>
</div>
