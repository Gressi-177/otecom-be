<div>
    <main id="main">
        <div class="page-header breadcrumb-wrap">
            <div class="container">
                <div class="breadcrumb">
                    <a href="/" rel="nofollow">Home</a>
                    <span></span> Thankyou

                </div>
            </div>
        </div>
        <div class="container pb-60 mt-30">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Thank you for your order</h2>
                    <lord-icon src="https://cdn.lordicon.com/sdhhsgeg.json" trigger="loop"
                        colors="primary:#4be1ec,secondary:#cb5eee" style="width:250px;height:250px">
                    </lord-icon>
                    <p>A confirmation email was sent.</p>
                    <a href="{{ route('shop') }}" class="btn btn-submit btn-submitx">Continue Shopping</a>
                </div>
            </div>
        </div>
        <!--end container-->

    </main>
</div>
