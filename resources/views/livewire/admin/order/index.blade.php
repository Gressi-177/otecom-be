<div>
    <div>
        <!-- Modal -->
        {{-- <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Category Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form wire:submit.prevent="destroyCategory">
                        <div class="modal-body">
                            <h6>Are you sure you want to delete this data?</h6>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Yes. Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> --}}

        <div class="pd-20 card-box mb-30">
            <div class="clearfix mb-20">
                <div class="pull-left">
                    <h4 class="text-blue h4">Order</h4>
                    @if (session('message'))
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                </div>
            </div>
            @if (Session::has('order_message'))
                <div class="alert alert-success" role="alert">{{ Session::get('order_message') }}</div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Sub total</th>
                        <th scope="col">Tax</th>
                        <th scope="col">Total</th>
                        <th scope="col">Username</th>
                        <th scope="col">Address</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Status</th>
                        <th scope="col">Order Date</th>
                        <th scope="col" colspan="2" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <th scope="row">{{ $order->id }}</th>
                            <td>{{ number_format($order->subtotal, 0, '', ',') }}đ </td>
                            <td>{{ number_format($order->tax, 0, '', ',') }}đ </td>
                            <td>{{ number_format($order->total, 0, '', ',') }}đ </td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->address }}</td>
                            <td>{{ $order->phone }}</td>
                            <td> <span class="badge badge-success">{{ $order->status }}</span></td>
                            <td>{{ $order->created_at }}</td>
                            <td style="width:185px">
                                <a href="{{ route('order.detail', ['order' => $order->id]) }}" type="button"
                                    class="btn btn-sm btn-info">
                                    View
                                </a>
                                <div class="btn-group dropdown">
                                    <button type="button" class="btn btn-sm btn-success dropdown-toggle waves-effect"
                                        data-toggle="dropdown" aria-expanded="false">
                                        Status <span class="caret"></span>
                                    </button>
                                    <div class="dropdown-menu" style="">
                                        <a class="dropdown-item" href="#"
                                            wire:click.prevent="updateOrderStatus({{ $order->id }},'delivered')">Delivered</a>
                                        <a class="dropdown-item" href="#"
                                            wire:click.prevent="updateOrderStatus({{ $order->id }},'canceled')">Canceled</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            <div>{{ $orders->links() }}</div>

        </div>
        {{-- @push('script')
            <script>
                window.addEventListener('close-modal', event => {
                    $('#deleteModal').modal('hide');
                })
            </script>
        @endpush --}}
    </div>

</div>
