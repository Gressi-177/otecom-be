<div>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">user Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="destroyUser">
                    <div class="modal-body">
                        <h6>Are you sure you want to delete this data?</h6>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Yes. Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">User</h4>
                @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                @endif
            </div>
            <div class="pull-right">
                <a href="{{ url('admin/users/create') }}" class="btn btn-primary btn-sm scroll-click" rel="content-y"
                    role="button" aria-expanded="true"><i class="fa fa-plus"></i> Add User</a>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr class="text-center">
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Image</th>
                    <th scope="col">Role_as</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="text-center">

                            <img src="{{ $user->image }}" style="width: 70px;height: 70px;" alt="user">
                        </td>
                        <td class="text-center"><span
                                class="badge badge-success">{{ $user->role_as == 1 ? 'Admin' : 'User' }}</span></td>
                        <td class="text-center">
                            <a href="{{ url('/admin/users/' . $user->id . '/edit') }}"
                                class="btn btn-sm btn-success">Edit</a>
                            <a href="#" wire:click="deleteUser({{ $user->id }})" class="btn btn-sm btn-danger"
                                data-toggle="modal" data-target="#deleteModal">Delete</a>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div>{{ $users->links() }}</div>
    </div>
    @push('script')
        <script>
            window.addEventListener('close-modal', event => {
                $('#deleteModal').modal('hide');
            })
        </script>
    @endpush
</div>
