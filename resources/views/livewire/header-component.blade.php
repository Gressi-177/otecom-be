<div>
    <header class="header-area header-style-1 header-height-2">
        <div class="header-top header-top-ptb-1 d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-3 col-lg-4">
                        <div class="header-info">
                            <ul>
                                <li>
                                    <a class="language-dropdown-active" href="#"> <i class="fi-rs-world"></i>
                                        English <i class="fi-rs-angle-small-down"></i></a>
                                    <ul class="language-dropdown">
                                        <li><a href="#"><img src="{{ asset('fe/assets/imgs/theme/flag-fr.png') }}"
                                                    alt="">Français</a></li>
                                        <li><a href="#"><img src="{{ asset('fe/assets/imgs/theme/flag-dt.png') }}"
                                                    alt="">Deutsch</a></li>
                                        <li><a href="#"><img src="{{ asset('fe/assets/imgs/theme/flag-ru.png') }}"
                                                    alt="">Pусский</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-4">
                    </div>
                    <div class="col-xl-3 col-lg-4">
                        <div class="header-info header-info-right">

                            @auth
                                <ul>
                                    <li>
                                        <i class="fi-rs-user"></i>{{ Auth::user()->name }} /
                                        <form method="post" action="{{ route('logout') }}">
                                            @csrf
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault(); this.closest('form').submit();">Logout</a>
                                        </form>
                                    </li>
                                </ul>
                            @else
                                <ul>
                                    <li>
                                        <i class="fi-rs-key"></i><a href="{{ route('login') }}">Log In </a> / <a
                                            href="{{ route('register') }}">Sign
                                            Up</a>
                                    </li>
                                </ul>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle header-middle-ptb-1 d-none d-lg-block">
            <div class="container">
                <div class="header-wrap">
                    <div class="logo logo-width-1">
                        <a href="/"><img src="{{ asset('fe/assets/imgs/logo/logo.png') }}" alt="logo"></a>
                    </div>
                    <div class="header-right">
                        <div class="search-style-1">
                            @livewire('header-search-component')
                        </div>
                        <div class="header-action-right">
                            <div class="header-action-2">

                                @livewire('wishlist-icon-component')

                                @livewire('cart-icon-component')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom header-bottom-bg-color sticky-bar">
            <div class="container">
                <div class="header-wrap header-space-between position-relative">
                    <div class="logo logo-width-1 d-block d-lg-none">
                        <a href="/"><img src="{{ asset('fe/assets/imgs/logo/logo.png') }}" alt="logo"></a>
                    </div>
                    <div class="header-nav d-none d-lg-flex">
                        <div class="main-categori-wrap d-none d-lg-block">
                            <a class="categori-button-active" href="#">
                                <span class="fi-rs-apps"></span> Browse Categories
                            </a>
                            <div class="categori-dropdown-wrap categori-dropdown-active-large">
                                <ul>
                                    @foreach ($categories as $category)
                                        <li>
                                            <a href=""> {{ $category->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="main-menu main-menu-padding-1 main-menu-lh-2 d-none d-lg-block">
                            <nav>
                                <ul>
                                    <li><a class="active" href="/">Home </a></li>
                                    <li><a href="{{ route('about') }}">About</a></li>
                                    <li><a href="{{ route('shop') }}">Shop</a></li>
                                    <li><a href="{{ route('contact') }}">Contact</a></li>
                                    <li><a href="">My Account<i class="fi-rs-angle-down"></i></a>
                                        @auth

                                            <ul class="sub-menu">
                                                <li><a href="{{ route('profile.edit') }}">Profile</a></li>
                                                @if (Auth::user()->role_as == '1')
                                                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                                @else
                                                    <li><a href="{{ route('shop.cart') }}">Cart</a></li>
                                                    <li><a href="{{ route('user.orders') }}">My order</a></li>
                                                @endif
                                                <li>
                                                    <form method="post" action="{{ route('logout') }}">
                                                        @csrf
                                                        <a href="{{ route('logout') }}"
                                                            onclick="event.preventDefault(); this.closest('form').submit();">Logout</a>
                                                    </form>
                                                </li>
                                            </ul>
                                        @else
                                            <ul class="sub-menu">
                                                <li><a href="{{ route('login') }}">Log In </a></li>
                                                <li><a href="{{ route('register') }}">Sign
                                                        Up</a></li>
                                            </ul>
                                        @endauth
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="hotline d-none d-lg-block">
                        <p><i class="fi-rs-smartphone"></i><span>Toll Free</span> (+1) 0000-000-000 </p>
                    </div>
                    {{-- <p class="mobile-promotion">Happy <span class="text-brand">Mother's Day</span>. Big Sale Up to 40%
                        </p> --}}
                    <div class="header-action-right d-block d-lg-none">
                        <div class="header-action-2">
                            <div class="header-action-icon-2">
                                @livewire('wishlist-icon-component')
                            </div>
                            <div class="header-action-icon-2">

                                @livewire('cart-icon-mobile-component')
                            </div>
                            <div class="header-action-icon-2 d-block d-lg-none">
                                <div class="burger-icon burger-icon-white">
                                    <span class="burger-icon-top"></span>
                                    <span class="burger-icon-mid"></span>
                                    <span class="burger-icon-bottom"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile-header-active mobile-header-wrapper-style">
        <div class="mobile-header-wrapper-inner">
            <div class="mobile-header-top">
                <div class="mobile-header-logo">
                    <a href="/"><img src="{{ asset('fe/assets/imgs/logo/logo.png') }}" alt="logo"></a>
                </div>
                <div class="mobile-menu-close close-style-wrap close-style-position-inherit">
                    <button class="close-style search-close">
                        <i class="icon-top"></i>
                        <i class="icon-bottom"></i>
                    </button>
                </div>
            </div>
            <div class="mobile-header-content-area">
                <div class="mobile-search search-style-3 mobile-header-border">
                    @livewire('header-search-mobile-component')
                </div>
                <div class="mobile-menu-wrap mobile-header-border">
                    <div class="main-categori-wrap mobile-header-border">
                        <a class="categori-button-active-2" href="#">
                            <span class="fi-rs-apps"></span> Browse Categories
                        </a>
                        <div class="categori-dropdown-wrap categori-dropdown-active-small">
                            <ul>
                                @foreach ($categories as $category)
                                    <li><a href="{{ route('product.category', ['slug' => $category->slug]) }}"><i
                                                class="surfsidemedia-font-dress"></i>{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- mobile menu start -->
                    <nav>
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children"><span class="menu-expand"></span><a
                                    href="/">Home</a></li>
                            <li class="menu-item-has-children"><span class="menu-expand"></span><a
                                    href="{{ route('shop') }}">shop</a></li>
                            <li class="menu-item-has-children"><span class="menu-expand"></span><a
                                    href="blog.html">Blog</a></li>
                            <li class="menu-item-has-children"><span class="menu-expand"></span><a
                                    href="#">Language</a>
                                <ul class="dropdown">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">French</a></li>
                                    <li><a href="#">German</a></li>
                                    <li><a href="#">Spanish</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- mobile menu end -->
                </div>
                <div class="mobile-header-info-wrap mobile-header-border">
                    @auth
                        <ul>
                            <li>
                                <div class="single-mobile-header-info">
                                    <a href="">
                                        <i class="fi-rs-user"></i>{{ Auth::user()->name }}
                                    </a>

                                </div>
                                <div class="single-mobile-header-info">
                                    <form method="post" action="{{ route('logout') }}">
                                        @csrf

                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault(); this.closest('form').submit();">Logout</a>

                                    </form>
                                </div>
                            </li>
                        </ul>
                    @else
                        <div class="single-mobile-header-info">
                            <a href="{{ route('login') }}">Log In </a>
                        </div>
                        <div class="single-mobile-header-info">
                            <a href="{{ route('register') }}">Sign Up</a>
                        </div>
                        @endif
                        <div class="single-mobile-header-info">
                            <a href="#">(+1) 0000-000-000 </a>
                        </div>
                    </div>
                    <div class="mobile-social-icon">
                        <h5 class="mb-15 text-grey-4">Follow Us</h5>
                        <a href="#"><img src="{{ asset('fe/assets/imgs/theme/icons/icon-facebook.svg') }}"
                                alt=""></a>
                        <a href="#"><img src="{{ asset('fe/assets/imgs/theme/icons/icon-twitter.svg') }}"
                                alt=""></a>
                        <a href="#"><img src="{{ asset('fe/assets/imgs/theme/icons/icon-instagram.svg') }}"
                                alt=""></a>
                        <a href="#"><img src="{{ asset('fe/assets/imgs/theme/icons/icon-pinterest.svg') }}"
                                alt=""></a>
                        <a href="#"><img src="{{ asset('fe/assets/imgs/theme/icons/icon-youtube.svg') }}"
                                alt=""></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
