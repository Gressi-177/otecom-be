<div>
    @if ($paginator->hasPages())
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-start">
                @foreach ($elements as $element)
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="page-item active"><a class="page-link"
                                        wire:click="gotoPage({{ $page }})">{{ $page }}</a></li>
                            @else
                                <li class="page-item"><a class="page-link"
                                        wire:click="gotoPage({{ $page }})">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                @if ($paginator->hasMorePages())
                    <li class="page-item"><a class="page-link" wire:click="nextPage"><i
                                class="fi-rs-angle-double-small-right"></i></a>
                    </li>
                @else
                    <li class="w-16 px-2 py-1 text-center rounded border bg-gray-200">Next</li>
                @endif
            </ul>
        </nav>
    @endif
</div>
