<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Confirmation</title>
</head>

<body>
    <p>Hi {{ $order->name }}</p>
    <p>Your order has been successfully placed.</p>
    <br />
    <table style="width:600px;text-align:right">
        <thead>
            <th>Image</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
        </thead>
        <tbody>
            @foreach ($order->orderItems as $item)
                <tr>
                    <td><img src="{{ $item->product->productImages[0]->image_url }}" width="100" alt=""></td>
                    <td>{{ $item->product->name }}</td>
                    <td>{{ $item->product->quantity }}</td>
                    <td>{{ $item->price * $item->quantity }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3"></td>
                <td style="font-size:15px;font-weight:bold"> Subtotal :
                    {{ number_format($order->subtotal, 0, '', ',') }}đ
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td style="font-size:15px;font-weight:bold">Tax : {{ number_format($order->tax, 0, '', ',') }}đ</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td style="font-size:15px;font-weight:bold">Shipping : Free Shipping</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td style="font-size:15px;font-weight:bold">Total : {{ number_format($order->total, 0, '', ',') }}đ</td>
            </tr>
        </tbody>
    </table>
</body>

</html>
