<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/categories",
     *     tags={"category"},
     *     summary="Get list categories",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     )
     * )
     */
    public function getCategories(Request $request)
    {
        $limit = $request->input('limit', 10);
        $query = Category::query();
        if ($sorting =  $request->input('sorting')) {
            $query->orderBy('created_at', $sorting);
        }
        $data = $query->take($limit)->get();
        return CategoryResource::collection($data);
    }
}
