<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SliderFormRequest;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use JD\Cloudder\Facades\Cloudder;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.slider.index', compact('sliders'));
    }

    public function create()
    {
        return view('admin.slider.create');
    }

    public function store(SliderFormRequest $request)
    {
        $validatedData = $request->validated();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $publicId = 'otecom/uploads/' . date('Y-m-d') . '-' . time();
            Cloudder::upload($file, $publicId);
            list($width, $height) = getimagesize($file);
            $validatedData['image_name'] = $publicId;
            $validatedData['image_url'] = Cloudder::show(Cloudder::getPublicId(), ['width' => $width, 'height' => $height]);
        }

        Slider::create([
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'image_name' => $validatedData['image_name'],
            'image_url' => $validatedData['image_url'],
            'status' => $validatedData['status'] == true ? '0' : '1',
        ]);

        return redirect('admin/sliders')->with('message', 'Slider Added Successfully');
    }

    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.slider.edit', compact('slider'));
    }

    public function update(SliderFormRequest $request, Slider $slider)
    {
        $validatedData = $request->validated();
        if ($request->hasFile('image')) {
            $image_name =  $slider->image_name;
            if ($image_name) {
                Cloudder::destroyImage($slider->image_name);
            }

            $file = $request->file('image');
            $publicId = 'otecom/uploads/' . date('Y-m-d') . '-' . time();
            Cloudder::upload($file, $publicId);
            list($width, $height) = getimagesize($file);
            $slider->image_name = $publicId;
            $slider->image_url = Cloudder::show(Cloudder::getPublicId(), ['width' => $width, 'height' => $height]);
        }

        $slider->title = $validatedData['title'];
        $slider->description = $validatedData['description'];
        $slider->status = $validatedData['status'] == true ? '0' : '1';
        $slider->save();
        return redirect('admin/sliders')->with('message', 'Slider Updated Successfully');
    }
}
