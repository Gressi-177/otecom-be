<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function totalRevenus($q = '')
    {
        if ($q == 'month') {
            return Order::whereMonth('created_at', date('m'))->sum('total');
        } else if ($q == 'day') {
            return Order::whereDay('created_at', date('d'))->sum('total');
        } else if ($q == 'year') {
            return Order::whereYear('created_at', date('Y'))->sum('total');
        } else if ($q == 'week') {
            return Order::where('created_at', '>=', Carbon::now()->startOfWeek()->format('Y/m/d'))->sum('total');
        }
        return Order::all()->sum('total');
    }

    public function totalOrder()
    {
        return Order::all()->count();
    }

    public function totalUsers()
    {
        return User::all()->count();
    }

    public function totalProducts()
    {
        return Product::all()->sum('quantity');
    }

    public function index()
    {
        $total_revenus = $this->totalRevenus();
        $total_month_revenus = $this->totalRevenus('month');
        $total_week_revenus = $this->totalRevenus('week');
        $total_year_revenus = $this->totalRevenus('year');
        $total_day_revenus = $this->totalRevenus('day');
        $total_order = $this->totalOrder();
        $total_users = $this->totalUsers();
        $total_products = $this->totalProducts();
        $latest_Order = OrderItem::latest()->paginate(4);
        return view('admin.dashboard', compact('total_revenus', 'total_month_revenus', 'total_week_revenus', 'total_year_revenus', 'total_day_revenus', 'total_order', 'total_users', 'total_products', 'latest_Order'));
    }
}
