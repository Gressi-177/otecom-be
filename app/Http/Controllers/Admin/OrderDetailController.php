<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderDetailController extends Controller
{

    public function index(int $oderId)
    {
        $orderItem = OrderItem::where('order_id', $oderId)->get();
        $order = Order::where('id', $oderId)->first();
        return view('admin.orderItem.index', compact('orderItem', 'order'));
    }
}
