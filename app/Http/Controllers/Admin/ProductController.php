<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductFormRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use JD\Cloudder\Facades\Cloudder;

class ProductController extends Controller
{
    public function index()
    {

        return view('admin.product.index');
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', compact('categories'));
    }

    public function store(ProductFormRequest $request)
    {
        $validatedData = $request->validated();

        $category = Category::findOrFail($validatedData['category_id']);

        $product = $category->products()->create([
            'category_id' => (int)$validatedData['category_id'],
            'name' => $validatedData['name'],
            'slug' => Str::slug($validatedData['name']),
            'small_description' => $validatedData['small_description'],
            'description' => $validatedData['description'],
            'original_price' => $validatedData['original_price'],
            'selling_price' => $validatedData['selling_price'],
            'quantity' => $validatedData['quantity'],
            'trending' => $request->trending ? '1' : '0',
            'status' => $request->status ? '0' : '1',
            'meta_title' => $validatedData['meta_title'],
            'meta_description' => $validatedData['meta_description'],
            'meta_keyword' => $validatedData['meta_keyword']

        ]);

        if ($request->hasFile('image')) {

            $i = 1;
            foreach ($request->file('image') as $imageFile) {
                $publicId = 'otecom/uploads/' . date('Y-m-d') . '-' . time() . '(' . $i++ . ')';
                Cloudder::upload($imageFile, $publicId);
                list($width, $height) = getimagesize($imageFile);
                $product->productImages()->create(
                    [
                        'product_id' => $product->id,
                        'image_name' => $publicId,
                        'image_url' => Cloudder::show(Cloudder::getPublicId(), ['width' => $width, 'height' => $height])
                    ]
                );
            }
        };
        return redirect('admin/products')->with('message', 'Product Added Successfully');
    }

    public function edit(int $product_id)
    {
        $product = Product::findOrFail($product_id);
        $categories = Category::all();
        return view('admin/product/edit', compact('product', 'categories'));
    }

    public function update(ProductFormRequest $request, int $product_id)
    {
        $validatedData = $request->validated();
        $product = Category::findOrFail($validatedData['category_id'])
            ->products()->where('id', $product_id)->first();

        if ($product) {
            $product->update([
                'category_id' => (int)$validatedData['category_id'],
                'name' => $validatedData['name'],
                'slug' => Str::slug($validatedData['name']),
                'small_description' => $validatedData['small_description'],
                'description' => $validatedData['description'],
                'original_price' => $validatedData['original_price'],
                'selling_price' => $validatedData['selling_price'],
                'quantity' => $validatedData['quantity'],
                'trending' => $request->trending == true ? '1' : '0',
                'status' => $request->status == true ? '0' : '1',
                'meta_title' => $validatedData['meta_title'],
                'meta_description' => $validatedData['meta_description'],
                'meta_keyword' => $validatedData['meta_keyword']
            ]);

            if ($request->hasFile('image')) {
                $i = 1;
                foreach ($request->file('image') as $imageFile) {
                    $publicId = 'otecom/uploads/' . date('Y-m-d') . '-' . time() . '(' . $i++ . ')';
                    Cloudder::upload($imageFile, $publicId);
                    list($width, $height) = getimagesize($imageFile);
                    $product->productImages()->create(
                        [
                            'product_id' => $product->id,
                            'image_name' => $publicId,
                            'image_url' => Cloudder::show(Cloudder::getPublicId(), ['width' => $width, 'height' => $height])
                        ]
                    );
                }
            };

            return redirect('admin/products')->with('message', 'Product Updated Successfully');
        } else {
            return redirect('admin/products')->with('message', 'No such Product Id Found');
        }
    }

    public function destroyImage($id)
    {
        $image =  ProductImage::findOrFail($id);
        Cloudder::destroyImage($image->image_name);
        $image->delete();
        return response()->json(
            [
                'message' => 'Deleted image successfully'
            ]
        );
    }
}
