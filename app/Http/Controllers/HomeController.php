<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $slider = Slider::all();

        return view('FE.index', compact('categories', 'slider'));
    }

    public function searchProducts(Request $request)
    {
        $query = Product::query();
        if ($keyword = $request->input('keyword')) {
            $query->where('name', 'like', "%$keyword%");
        }
        if ($order = $request->input('order')) {
            $query->orderBy($order, 'asc');
        }
        $inStock = $request->input('inStock', 0);
        if ($inStock == 1) {
            $query->where('quantity', '>', 0);
        }
        $limit = $request->input('limit', 8);
        if ($page = $request->input('page')) {
            $data = $query->paginate($limit);
            return $data;
        }

        $data = $query->take($limit)->get();
        return $data;
    }
}
