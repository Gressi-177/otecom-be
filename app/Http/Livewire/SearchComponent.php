<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use DB;
use Livewire\Component;
use Livewire\WithPagination;

class SearchComponent extends Component
{
    use WithPagination;

    public $limit = 9;
    public $orderBy = "Default sorting";

    public $q;
    public $search_term;

    public function mount()
    {
        $this->fill(request()->only('q'));
        $this->search_term = '%' . $this->q . '%';
    }

    public function changeLimit($limit)
    {
        $this->limit = $limit;
    }

    public function changeOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    public function render()
    {
        if ($this->orderBy == "Price: Low to High") {
            $products = Product::where('name', 'LIKE', $this->search_term)->orderBy('original_price', 'ASC')->paginate($this->limit);
        } else if ($this->orderBy == "Price: High to Low") {
            $products = Product::where('name', 'LIKE', $this->search_term)->orderBy('original_price', 'DESC')->paginate($this->limit);
        } else if ($this->orderBy == "Sort by Newness") {
            $products = Product::where('name', 'LIKE', $this->search_term)->orderBy('created_at', 'DESC')->paginate($this->limit);
        } else {
            $products = Product::where('name', 'LIKE', $this->search_term)->paginate($this->limit);
        }
        $categories = Category::orderBy('name', 'ASC')->get();

        $nproducts = Product::latest()->take(4)->get();

        return view('livewire.search-component', compact('products', 'categories', 'nproducts'));
    }
}
