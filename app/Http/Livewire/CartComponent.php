<?php

namespace App\Http\Livewire;

use Auth;
use Livewire\Component;
use Cart;
use Session;

class CartComponent extends Component
{
    public function increaseQuantity($rowId)
    {
        $product = Cart::instance('cart')->get($rowId);
        $qty = $product->qty + 1;
        Cart::instance('cart')->update($rowId, $qty);
        $this->emitTo('cart-icon-component', 'refreshComponent');
        $this->emitTo('cart-icon-mobile-component', 'refreshComponent');
    }

    public function decreaseQuantity($rowId)
    {
        $product = Cart::instance('cart')->get($rowId);
        $qty = $product->qty - 1;
        Cart::instance('cart')->update($rowId, $qty);
        $this->emitTo('cart-icon-component', 'refreshComponent');
        $this->emitTo('cart-icon-mobile-component', 'refreshComponent');
    }

    public function destroy($id)
    {
        Cart::instance('cart')->remove($id);
        Session::flash('success', 'Item has been removed!');
        $this->emitTo('cart-icon-component', 'refreshComponent');
        $this->emitTo('cart-icon-mobile-component', 'refreshComponent');
    }

    public function clearAll()
    {
        Cart::instance('cart')->destroy();
        $this->emitTo('cart-icon-component', 'refreshComponent');
        $this->emitTo('cart-icon-mobile-component', 'refreshComponent');
    }

    public function checkout()
    {
        if (Auth::check()) {
            return redirect()->route('checkout');
        } else {
            return redirect()->route('login');
        }
    }

    public function setAmountForCheckout()
    {
        if (!Cart::instance('cart')->count() > 0) {
            session()->forget('checkout');
            return;
        }
        session()->put('checkout', [
            'subtotal' => Cart::instance('cart')->subtotal(2, '.', ''),
            'tax' => Cart::instance('cart')->tax(2, '.', ''),
            'total' => Cart::instance('cart')->total(2, '.', ''),
        ]);
    }

    public function render()
    {
        $this->setAmountForCheckout();
        return view('livewire.cart-component');
    }
}
