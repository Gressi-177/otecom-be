<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use DB;
use Cart;
use Livewire\Component;
use Livewire\WithPagination;

class ShopComponent extends Component
{
    use WithPagination;

    public $limit = 9;
    public $orderBy = "Default sorting";
    public $minPrice = 0;
    public  $maxPrice = 'max';

    public function store($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('\App\Models\Product');
        session()->flash('success_message', 'Item added in cart');
        return redirect()->route('shop.cart');
    }

    public function changeLimit($limit)
    {
        $this->limit = $limit;
    }

    public function changeOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    public function fillByPrice($minPrice, $maxPrice)
    {
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    }

    public function addToWishlist($product_id, $product_name, $product_price)
    {
        Cart::instance('wishlist')->add($product_id, $product_name, 1, $product_price)->associate('\App\Models\Product');
        $this->emitTo('wishlist-icon-component', 'refreshComponent');
    }

    public function removeFromWishlist($product_id)
    {
        foreach (Cart::instance('wishlist')->content() as $witem) {
            if ($witem->id == $product_id) {
                Cart::instance('wishlist')->remove($witem->rowId);
                $this->emitTo('wishlist-icon-component', 'refreshComponent');
                return;
            }
        }
    }

    public function render()
    {
        $query = Product::query();
        if ($this->maxPrice != 'max') {
            $query->whereBetween('selling_price', [$this->minPrice, $this->maxPrice]);
        } else {
            $query->where('selling_price', '>', $this->minPrice);
        }
        if ($this->orderBy == "Price: Low to High") {
            $query->orderBy('original_price', 'ASC');
        } else if ($this->orderBy == "Price: High to Low") {
            $query->orderBy('original_price', 'DESC');
        } else if ($this->orderBy == "Sort by Newness") {
            $query->orderBy('created_at', 'DESC');
        }

        $products = $query->paginate($this->limit);
        $categories = Category::orderBy('name', 'ASC')->get();
        $totalProduct = DB::table('products')->count();
        $nproducts = Product::latest()->take(4)->get();

        return view('livewire.shop-component', compact('products', 'categories', 'totalProduct', 'nproducts'));
    }
}
