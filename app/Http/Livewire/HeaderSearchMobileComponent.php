<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HeaderSearchMobileComponent extends Component
{
    public function render()
    {
        return view('livewire.header-search-mobile-component');
    }
}
