<?php

namespace App\Http\Livewire\Admin\User;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    public $user_id;

    public function deleteUser($user_id)
    {
        return $this->user_id = $user_id;
    }

    public function destroyUser()
    {
        $user = User::find($this->user_id);
        // $image_name =  $user->image_name;
        // if ($image_name) {
        //     Cloudder::destroyImage($slider->image_name);
        // }
        $user->delete();
        \Session::flash('message', 'User Deleted');
        $this->dispatchBrowserEvent('close-modal');
    }

    public function render()
    {
        $users = User::orderBy('id', 'DESC')->paginate(10);
        return view('livewire.admin.user.index', compact('users'));
    }
}
