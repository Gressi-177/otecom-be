<?php

namespace App\Http\Livewire\User;

use App\Models\Order;
use App\Models\OrderItem;
use Auth;
use DB;
use Livewire\Component;

class UserOrderDetailComponent extends Component
{
    public $order_id;

    public function mount($order_id)
    {
        $this->order_id = $order_id;
    }
    public function cancelOrder()
    {
        $order = Order::find($this->order_id);
        $order->status = 'cancelled';
        $order->canceled_date = DB::raw('CURRENT_DATE');
        $order->save();
        session()->flash('order_message', 'Order has been canceled!');
    }
    public function render()
    {
        $order = Order::where('user_id', Auth::User()->id)->where('id', $this->order_id)->first();
        $orderItem = OrderItem::where('order_id', $order->id)->get();
        return view('livewire.user.user-order-detail-component', compact('order', 'orderItem'));
    }
}
