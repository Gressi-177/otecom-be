<?php

namespace App\Http\Livewire;


use App\Models\Category;
use App\Models\Product;
use DB;
use Cart;
use Livewire\Component;
use Livewire\WithPagination;

class CategoryComponent extends Component
{
    use WithPagination;

    public $limit = 9;
    public $orderBy = "Default sorting";
    public $slug;
    public $minPrice = 0;
    public  $maxPrice = 'max';
    public function mount($slug)
    {
        $this->slug = $slug;
    }

    public function store($product_id, $product_name, $product_price)
    {
        Cart::add($product_id, $product_name, 1, $product_price)->associate('\App\Models\Product');
        session()->flash('success_message', 'Item added in cart');
        return redirect()->route('shop.cart');
    }

    public function changeLimit($limit)
    {
        $this->limit = $limit;
    }

    public function changeOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }
    public function fillByPrice($minPrice, $maxPrice)
    {
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    }

    public function render()
    {
        $category = Category::where('slug', $this->slug)->first();
        $category_id = $category->id;
        $category_name = $category->name;
        $query = Product::query();
        $query->where('category_id', $category_id);
        if ($this->maxPrice != 'max') {
            $query->whereBetween('selling_price', [$this->minPrice, $this->maxPrice]);
        } else {
            $query->where('selling_price', '>', $this->minPrice);
        }
        if ($this->orderBy == "Price: Low to High") {
            $query->orderBy('original_price', 'ASC');
        } else if ($this->orderBy == "Price: High to Low") {
            $query->orderBy('original_price', 'DESC');
        } else if ($this->orderBy == "Sort by Newness") {
            $query->orderBy('created_at', 'DESC');
        }
        $products = $query->paginate($this->limit);
        $categories = Category::orderBy('name', 'ASC')->get();
        $nproducts = Product::latest()->take(4)->get();

        return view('livewire.category-component', compact('products', 'categories', 'nproducts', 'category_name'));
    }
}
