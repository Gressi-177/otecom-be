<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use Livewire\Component;
use Cart;
use DB;

class HomeComponent extends Component
{
    public function store($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('\App\Models\Product');
        session()->flash('success_message', 'Item added in cart');
        return redirect()->route('shop.cart');
    }
    public function render()
    {
        $limit = 8;
        $nproducts =  Product::orderBy('created_at', 'ASC')->limit($limit)->get();
        $product_ids
            = DB::table('order_items')->selectRaw('count(product_id) as total_order, product_id')->groupBy('product_id')->orderBy('total_order', 'DESC')->limit(8)->pluck('product_id')->toArray();
        $ids = implode(',', $product_ids);

        $pproducts = Product::whereIn('id', $product_ids)
            ->orderByRaw(DB::raw("FIELD(id, $ids)"))
            ->take(8)
            ->get();

        $categories = Category::inRandomOrder()->limit(6)->get();
        $sliders = Slider::inRandomOrder()->get();
        return view('livewire.home-component', compact('nproducts', 'categories', 'pproducts', 'sliders'));
    }
}
