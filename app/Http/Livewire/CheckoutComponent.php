<?php

namespace App\Http\Livewire;

use App\Mail\OrderMail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipping;
use App\Models\Transaction;
use Auth;
use Livewire\Component;
use Cart;
use Mail;

class CheckoutComponent extends Component
{

    public $ship_to_different;

    public $name;
    public $address;
    public $phone;
    public $note;

    public $paymentmode;
    public $thankyou;

    public function mount(){
        if(Auth::check()){
            $this->name = Auth::user()->name;
            $this->address = Auth::user()->address;
            $this->phone = Auth::user()->phone;
        }
    }

    public function placeOrder()
    {
        $this->validate(
            [
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'paymentmode' => 'required'

            ]
        );

        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->subtotal = session()->get('checkout')['subtotal'];
        $order->tax = session()->get('checkout')['tax'];
        $order->total = session()->get('checkout')['total'];
        $order->name = $this->name;
        $order->address = $this->address;
        $order->phone = $this->phone;
        $order->note = $this->note;
        $order->note = 'ordered';
        $order->is_shipping_different = $this->ship_to_different ? 1 : 0;
        $order->save();

        if(Auth::check()){
            $user = Auth::user();
            $user->address = $this->address;
            $user->phone = $this->phone;
            $user->save();
        }

        foreach (Cart::instance('cart')->content() as $item) {
            $orderItem = new OrderItem();
            $orderItem->product_id = $item->id;
            $orderItem->order_id = $order->id;
            $orderItem->price = $item->price;
            $orderItem->quantity = $item->qty;
            $orderItem->save();
        }

        $shipping = new Shipping();
        $shipping->order_id = $order->id;
        $shipping->name = $this->name;
        $shipping->address = $this->address;
        $shipping->phone = $this->phone;
        $shipping->note = $this->note;
        $shipping->save();

        if ($this->paymentmode == 'cod') {
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->order_id = $order->id;
            $transaction->mode = 'cod';
            $transaction->status = 'pending';
            $transaction->save();
        }

        $this->thankyou = 1;
        Cart::instance('cart')->destroy();
        session()->forget('checkout');
        $this->sendOrderConfimation($order);
    }

    public function verifyForCheckout()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        } else if ($this->thankyou == 1) {
            return redirect()->route('thankyou');
        } else if (!session()->get('checkout')) {
            return redirect()->route('product.cart');
        }
    }

    public function sendOrderConfimation($order)
    {
        Mail::to(Auth::user()->email)->send(new OrderMail($order));
    }

    public function render()
    {
        $this->verifyForCheckout();
        return view('livewire.checkout-component');
    }
}
